export const mockIntegrationProps = {
  id: 25,
  projectId: 1,
  operating: true,
  personalAccessTokensPath: '/path/to/personal/access/tokens',
  editable: true,
  googleArtifactManagementProps: {
    artifactRegistryPath: '/path/to/artifact/registry',
  },
  sections: [],
  fields: [{ name: 'artifact_registry_project_id', value: null }],
};
